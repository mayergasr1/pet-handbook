---
title: Pinewood Emergency Team Handbook
---

<div align="center">

![PET Logo|150x150, 100%](/PET-Logo.png)
# [Pinewood Emergency Team](https://www.roblox.com/groups/2593707/Pinewood-Emergency-Team/about)

</div>

The following document is the handbook of the Pinewood Emergency Team, written by the Specialists and the Chief. Please read it carefully. Any changes to this handbook will be made clear.

Welcome to the Pinewood Emergency Team, an elite group of the best rescue heroes working together to respond to emergencies across all Pinewood facilities! Using high-tech equipment, we work hard to save countless amount of visitors every day! We primarily patrol the Pinewood Computer Core, our main homebase. 

:fire: **Above the flames!**

<hr>

![PET Area|690x373](/PET-Area.jpg)




<div align="center">

## ***General Reminders*** 

</div>

* Follow the [ROBLOX Community Rules ](https://en.help.roblox.com/hc/en-us/articles/203313410-Roblox-Community-Rules) and the [Pinewood Builders Game Rules ](https://devforum.roblox.com/t/pinewood-builders-game-rules/907637) **at all times**.
* Be sure to show respect to other peers; especially those who are of a higher rank than you.
* A certain degree of discipline and composure is expected from the attendee **at all Pinewood events** - listen to your superiors and treat all with respect. 
* We are not at war with Innovation, Quantum Science, or any other group for that matter. Active hostility against them are not tolerated!
* The act of **spawnkilling or mass-random killing**, whether you are on duty for PET or not, is outright against the rules, and if caught, will lead to a harsh punishment. <br>
  - Intentional teamkilling is also outright against the rules whilst on duty for PET and will also result in a punishment if caught.
* PET Respondents can **never** start fires or participate in chaos, this is contrary to our mission as PET, doing so could possibly get you considered rogue. 
* Being “on duty” is defined by a number of factors and includes many ins and outs - also read up on the handbooks of the [Pinewood Builders Security Team](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook-11-10-2019/385368) and [The Mayhem Syndicate ](https://devforum.roblox.com/t/the-mayhem-syndicate-handbook/595758) after you are finished here for the best possible grasp on the restrictions you must abide by.
* Specialists have the right to remove a ranked member from their position on the basis of bad behaviour from both the Communications Server and in-game. Poor behaviour includes toxicity, NSFW-like behaviour, etc. 
* Intentional use of Plagarism (e.g. stealing PB Assets or community-made work) in any way, shape or form is **strictly prohibited and frowned upon**, and will certainly lead to a punishment, *especially* if you are a ranked member. 

--- 

<div align="center">

## ***Going on-duty***

</div>

In order to be properly recognized as a PET respondent; you must fill in the proper requirements to be on-duty.

An on-duty PET respondent must have their ranktag set to PET group (this can be done with the in-game command “!setgroup pet”), an official PET uniform on at all times, as well as the handbook rules in their head. Seen below are the various examples of a PET respondent ready to go on-duty!

**An on-duty PET respondent may NEVER carry TMS or PBST tools, that is against the handbook. TMS and PBST are also not allowed to carry PET tools whilst on-duty.**

![image|690x358](/PET-on-duty.jpg)

---

<div align="center">

## ***Important Info***

</div>

The following outlines some important information for those who are on-duty for PET that are otherwise not specified on other sections of the handbook. 

<p>
<details>
<summary>Temperature/Core</summary>

One of PET’s responsibilities is controlling the temperature of the core, should it exceed 3000 degrees or dip below -3000. This section goes into more detail about the core rules.

When the core is at a temperature of above 3000 degrees, or one below -3000, it is considered to be in a **critical state**. At this point, the core is extremely close to either a meltdown or freezedown event and PET is permitted to access the PBCC’s temperature changing controls.

PET members are permitted to cool/heat the core from the mentioned critical status temperature, to 2000/-2000 degrees.

When cooling from 3000, be sure to especially utilise the use of fans, coolant and the coolant pipe!

During TMS raids, PET are additionally permitted to cool/heat the core from critical temperature, to 1000/-1000 degrees!

![531e954344aad72a550c5f0f964921e9|690x370](/PET-core.jpg)

</details>
</p>

<p>
<details>
<summary>Events</summary>

Every once in a while, a disaster will happen at the Pinewood Computer Core. These disasters can change the gameplay's state tremoudously. As an on-duty respondent for PET, it is your job and responsbility to ensure that no further damage occurs and heal anyone injured as a result of said disasters. Below goes into more detailing on what to do.

* **Meltdown** - Attempt to successfully fill all 3 rods for the best chance to revert a meltdown. If it isn't possible, evacuate visitors via the Launch Silos or the PBRF elevators.  
* **Freezedown** -  Evacuate visitors via the Launch Silos or the PBRF elevators.  
* **Gas Leak** - Switch all 5 fans on in order for the gas to be blown out in a quicker manner, heal anyone injured! 
* **Earthquake** - Attempt to halt all trains in order to prevent further damage. 
* **Plasma Leak** - Evacuate as much people out of the core as possible, heal anyone injured!
* **Blackout** - Switch on the emergency generators as soon as possible, whilst also ensuring that there is enough generator fuel for this.
* **Radiation Leak** - Drain the hazmat by opening all of the 4 drainage plugs at the same time.
* **Magma** - Evacuate everyone to higher grounds or the lobby.
* **Aliens** - Not a dangerous event.
* **???** - ???

![image|690x369, 100%](/PET-events.jpg)

</details>
</p>

<p>
<details>
<summary>Trains and debris</summary>

Very often whilst patrolling, you will find a lot of trains derailed throughout the facility. One of PET's duties is to ensure the train line is as clear as possible and disruption is kept to a minimum. 

If any train debris (or other debris) manages to reach the tracks, try your best to remove it as quickly as possible! One tip is to use the personal rocketship and blast the debris away, it can sometimes do the job! 

If a train driver derails their train and they are still seated, try to ask them if they can remove their train. 

If there is a nuke train whose rockets are about to blow up in a busy and populated area, and the train driver refuses to delete their train and remains seated, you are free to kill said driver in order to protect others. 

***IMPORTANT:*** **DO NOT use your weaponary to fire shots at a driver whose train is derailed. That should only ever be used if the driver is threatening you with their own weapons OR if the driver is causing a huge backlog of traffic and is refusing to delete their train, even after being asked multiple times.**

**A PET Respondent can never drive nuke trains, as well as lighting nuke trains, under any cirumstances.**

![PBCC NUKE TRAIN|690x367](/PBCC-nuke-train.jpg)

</details>
</p>

<p>
<details>
<summary>E-Coolant</summary>

Emergency Coolant, abbreviated as E-Coolant, is a PBCC gameplay mechanic located at Sector G that allows users to have a last-minute chance at saving the core once a Meltdown begins. 

In order to access both doors at Sector G, you need to input the code **5334118**.

E-Coolant plays an important role for PET, as it can prevent the chance of total facility destruction! Therefore, the E-Coolant must be kept under PET's (and PBST if they are present) control! If successful, the E-Coolant will set the temp back to 3,500.

To achieve meltdown reversion through the E-Coolant procedure; at least *one* of the rods must have their levels between **69 and 81** percent to be green. Going above or below those ranges will make the rod unusable.

The more rods filled, the higher chance there is for the temperature to be set back to 3,500.

Success Rates for the number of tanks green when the countdown reaches *0* <br>
1 rod - 10% <br>
2 rods - 25% <br>
3 rods - 90%

![31d6088079cf9955ed2c800e09c8452c|690x369](/PBCC-E-Coolant.jpg)

</details>
</p>

<p>
<details>
<summary>Dealing with Rogue Respondents</summary>

From time to time, you may find yourself witnessing a rogue on-duty PET personnel whilst patrolling. Usually they are of no threat to you, but sometimes they may break major rules, such as:

- Teamkilling intentionally
- Spawnkilling and MRKing, 
- Killing innocent neutrals for no reasons,  
- Igniting nuke trains and lighting fires,
- Intentionally causing a train backlog,
- Killing pro-core members on purpose,
- Intentionally violating the PET Handbook.

You may have to fight them if they attack you. 

If you witness a rogue PET member, be sure to first tell them of their wrongdoings; they may not even know that they're breaking the handbook rules. Should they continue even after that warning, however, request a Kill-On-Sight order on the grounds that the user is rogue to a member ranked Marshall or above. 

***`If you witness a ranked Trained Respondent+ going rogue and breaking major rules; send a PIA call. Be descriptive in the notes and type what exactly they are doing.`***

</details>
</p>

<p>
<details>
<summary>Dealing with exploiters</summary>

From time to time whilst patrolling; you may find yourself encoutering an exploiter. Should this happen, send a PIA call using the !call command in chat, note down the name of the exploiter and add a description if you can. Until the PIA arrives, you are free to kill the exploiter(s).

It is also worth it to record any evidence, if possible, of the exploiter hacking so you can send it to a PIA moderator in the event that the exploiter left before the PIA arrives. 

![905cbfe6748afd53d5be8fbb8c7d7e9b|690x368](/PBCC-PIA-Room.jpg)

</details>
</p>

<p>
<details>
<summary>Mutants</summary>

Mutants are on automatic Kill-On-Sight; you are free to kill them if you spot one.

</details>
</p>

---

<div align="center">

## ***Ranks***

</div>

Ranking up in PET is simple. Once you earn enough points and meet the requirements of the rank, you will be ranked up. There are evaluations in place to make sure PET only has only the best within their ranks. A higher rank gives you higher authority and enhanced permissions in PET, as well as minor gameplay buffs to the PET loadout med-kit.

<p>
<details>
<summary>Rescue Hero (LR)</summary>

#### Rescue Hero

The entry rank for PET. Rescue Heroes are expected to look up to the higher ranks, and take them as an example of how to act. There is no evaluation for this rank.

**Tools:** 
- A PET Riot Shield: This defends against rifles, SMG and pistol shots, and makes a small clank sound when hit by a bullet.
- A PET Fire Hose: This is a very powerful tool and produces a very huge water spray effect. It is much better than than the normal fire extinguisher.
- A PET Med-Kit: This is a very powerful tool and operates by the player holding it out and then walking into the target player who needs to be healed. The healing stats for this item increases the more you rank up!
- A PET Disasters Tablet: This tablet provides a huge array of useful information for PET Respondents patrolling, which includes how many vehicles are spawned across the map, if the cargo bays are on fire, if the transit system is safe, and much more! 

</details>
</p>

<p>
<details>
<summary>Trained Respondent (LR)</summary>

#### Trained Respondent

The first rank that one can progress to through an evaluation. It is a big turning point for anyone participating in PET. Trained Respondents are held to a much, much higher standard than the average Rescue Hero. Trained Respondents are trusted with the ability to make meaningful PET calls.

**Requirements:** 50 points + pass Trained Respondent Evaluation

**Evaluation - Takes place at PBSTAC**

* **Phase 1 -** Fire simulation activity, 5 flaming parts will be spawned with a cooldown of half a second between each part. Protect the cargo and extinguish all fires within 30 seconds.
* **Phase 2 -** Avoid damage from the juggernaut’s shots for 30 seconds. You will have 150 HP.
* **Phase 3 -** Answer 5 basic questions about the PET handbook and our duties around the PBCC. You only have to get 4 correct, but should really be aiming for 100%.
* **Phase 4 -** Pass a consensus from one Specialist other than the evaluator.

**Special Permissions**
* Access to an improved PET med-kit. This med-kit heals **3 HP per second** .
* Access to the in-game “!call pet” command.
* Access to write in the tips for duties channel in the comms server.
* Access to ranked announcements and a ranked chat for Trained Respondent+ in the comms server.

</details>
</p>

<p>
<details>
<summary>Dedicated Respondent (MR)</summary>

#### Dedicated Respondent

The second rank achievable within PET. By the time one reaches the rank of Experienced Respondent, they are expected to know the ins and outs of PET operations in-game. Dedicated personnel should expect to be seen as an example.

**Requirements:** 150 points + pass Dedicated Respondent evaluation

**Evaluation - Takes place at PBSTAC**

* *You must first be a Trained Respondent for **two weeks** before you can evaluate up to this rank.*
* **Phase 1 -** Fire simulation activity, 7 flaming parts will be spawned with a cooldown of half a second between each part. Protect the cargo and extinguish all fires.
* **Phase 2 -** Complete level 1 of the “Bomb Barrage” simulation activity.
* **Phase 3 -** Get 10 hits within 50 seconds at the firing range.
* **Phase 4 -** Pass a consensus from two Specialists other than the evaluator.


**Special Permissions**

* All of the Trained Respondent's permissions.
* Access to an improved PET med-kit. This med-kit heals **4.5 HP per second** .
* Given access to the Reward Request system via the PET Communication Server. 

</details>
</p>

<p>
<details>
<summary>Elite Respondent (MR)</summary>

#### Elite Respondent

The third and final rank in the main progression. For the regular participant, this is the top. Elites are to be the best examples for aspiring emergency personnel and their actions must reflect this at all times, especially when hosting group patrols and other events. 

*As an Elite Respondent, you will need access to the PET Communication Server if you wish to host an event!* 

**Requirements:** 300 points + pass Elite Respondent evaluation

**Evaluation - Takes place at PBCC**

* *You must first be a Dedicated Respondent for **three weeks** before you can evaluate up to this rank.*
* **Phase 1 -** Refill coolant and generator fuel in one run of the cargo train. This must be completed in under 75 seconds.
* **Phase 2 -** Using the speed coil provided, turn off all core boost lasers and set the reactor power to 1 in under 35 seconds.
* **Phase 3 -** In 80 seconds or less, use the speed coil once again provided, fill up all three e-coolant pipes to a green capacity.
* **Phase 4-** A 5 question quiz relating to patrol knowledge and leadership. 
* **Phase 5 -** Pass a consensus from three Specialists other than the evaluator.

**Special Permissions**

* All of the Dedicated Respondent’s permissions.
* Access to an improved PET med-kit. This med-kit heals **10.3 HP per second** 
* Granted the distinction of hosting a PET Group Patrol event.
* Also granted the distinction of hosting a PET Patrol Support event.
* Given the authority to request deductions as well as points on PET personnel in a private channel. 
* Granted access to an exclusive channel for Elites and above in the Communication Server. 

</details>
</p>

<p>
<details>
<summary>Marshall (HR)</summary>

#### Marshall

The first HR rank in PET.  A Marshall has many enhanced permissions and responsibilities, which includes the ability to host pointed patrols, as well as moderation abilities in the communications server.

*As a marshall, you will need access to the PET Communication Server as well as having 2 factor authentication enabled!* 

**Requirements:** Handpicked for evaluation patrol

**Evaluation - Takes place at PBCC**

* You can be invited to Marshall from any rank, however being an Elite Respondent helps your chances greatly.
* Nominated to take an evaluation patrol by a Specialist+
* Evaluated patrol, observed by a Specialist.
* Passing consensus vote among current Specialists.


**Special Permissions**

* All of the Elite Respondent’s permissions.
* Access to an improved PET med-kit. This med-kit heals **12 healing per second** .
* Ability to mini and full patrols.
* If chosen, they are allowed to host a server during an Operation MEGA event.
* Issue KoS orders in a pinch.
* Override uniform requirements.
* Moderation powers in the PET communications server.
* Granted smartlog access to the PET database.
* Allowed to host gamenights with the approval of a Specialist.

</details>
</p> 

<p>
<details>
<summary>Specialist (HR)</summary>

#### Specialist

The second HR rank. Specialists are the leadership rank in PET and help the Chief manage the group. They vote on HR promotions, new PET policies, have the ultimate flexibility in terms of event hosting and more.

**Requirements:** Handpicked from Marshalls

**Evaluation**

* You first need to be nominated then voted in by all of the Specialists and the Chief. 

**Special Permissions**

* All of the Marshall's permissions
* Access to an improved PET med-kit. This med-kit heals **16 HP per second** .
* Specialists has full authority over lower-ranked members regarding all situations, ranging from handbook violations to KOS orders.
    - Specialist+ also has it on the highest authority to cancel any event where they deem that it is appropriate to do so and take any action necessary. 
* Allowed to host any type of PET event.
* Allowed to schedule and set up an Operation MEGA event.
* Allowed to schedule and set up a Community Patrol.
* Allowed to override any KOS orders for users below them in rank.
* Granted full access to the PET database, in order to organise points, promote members, and more.
* Allowed to wear the official PET leadership hoodie.
* Granted full administration access in the Comms server.
* Granted admin commands at the PET private servers of PBSTAC, PBCC and other PB facilities.
* Ability to vote and propose votes in PET.
* Granted direct communications with the Pinewood Intelligence Agency in a private channel.
* Vote with TMS and PBST leadership members regarding subgroup-wide votes.
*  Overall being responsible for all changes done to the group.

</details>
</p>

<p>
<details>
<summary>Chief (HR)</summary>

#### Chief

The final PET rank. This role is essentially the leader of PET and is held by one individual. neonweld is the current Chief. 

The Chief has command over the Specialists as well as veto powers regarding leadership votes.

</details>
</p>

---

For each rank with an evaluation; there is a chance that you may fail a phase. Should this happen, you will be given a phase cooldown by the evaluator before you are able to re-take the failed phase again. For the first time, the cooldown is typically 24 hours.

You only need to re-do phases in which you fail.


**You should not bribe a high rank for free points and/or ranks.**

---

<div align="center">

## ***Respondents of the Month***

</div>

If you are a Respondent who shows exceptional behaviour and have made a positive impact in the group, you could be eligible for the monthly Respondent of the Month award! Every month, 2 to 3 members will be chosen for this award in order to recognise and congratulate members who have clearly shown that they are an amazing role model for others. Others should look up to those who are able to accomplish this role, as they clearly demonstrated that they are the best of the best! 

Those who gets this award also gets a cool and funky "Respondent of the Month" PET ranktag in-game! 

---

<div align="center">

## ***Events***

</div>

There are multiple event types that an attendee can get points from in PET. Events are hosted whenever a host would like to, but are usually scheduled well in advance. Depending on the length and type, anywhere from 2-5 PET points on average can be earned from one event.

*A special event called **OPERATION: MEGA** exists where you can earn **up to 10 points**!*

**Patrol Events:**

<p>
<details>
<summary>Group Patrol</summary>

Group Patrols are a new, frequently hosted event hosted by Elite Respondent+! This patrol type is mainly focused on patrolling in groups for fun and completing various objectives across PB facilities. An Elite Respondent+  will rally up PET members to a server, where they would act as a patrol leader, leading PET members during disasters and whatnot.

**One person can receive a point bonus during a Group Patrol for showing outstanding communication and/or skill!**

</details>
</p>

<p>
<details>
<summary>Mini Patrol</summary>

Mini Patrols are another normal patrol type of event hosted in PET. They take place in the public PBCC servers and can be hosted by Marshalls and above. A Mini Patrol can last up to 30 minutes. Due of their short duration, Mini Patrols are jam-packed with action and are common targets of attacks by The Mayhem Syndicate! 

**You can earn up to 3 PET points from a mini patrol, with the possible bonus of 4.**

</details>
</p>

<p>
<details>
<summary>Full Patrol</summary>

Full Patrols are the most classic and barebones event hosted in PET. They are hosted regularly by Marshalls and above and usually take place in public PBCC servers, though on special occasions another facility may be patrolled. Under regular circumstances, a patrol can last as long as one hour. 

**You can earn up to 5 PET points from a normal patrol, with a possible bonus of 6**.

</details>
</p>

<p>
<details>
<summary>Joint Patrol</summary>

This is another patrol type where PET and PBST join as a collective force and patrol together in one server! This can also be hosted by a Marshall or above, and you can expect to see more TMS then usual during this event! 

**You can earn up to 5 PET points from a joint patrol, with a possible bonus of 6.**
**PBST's point scales are different depending on what level their patrol is, check the PBST handbook for more information on their point scales.** 

</details>
</p>

<p>
<details>
<summary>PBST Patrol Support</summary>

Patrol Support events are very different from the other types of patrols hosted in PET in the sense that, as the name suggests, this patrol type **is solely focused on PBST patrols**. The intent is to primarily heal PBST members throughout the entire patrol duration in order to boost and strengthen their defence during bad situations, examples of such being a full-out battle against the Syndicates. By doing this correctly, it will **reduce the chances** of PBST members **having a lower HP than usual.** 

These events can be hosted by an Elite Respondent+, and the priority objective is to stick by PBST and heal them as much as PET can when they lose HP, however other objectives can be accomplished if this objective is met consistently.

</details>
</p>




---

## Special Events:

<p>
<details>
<summary>Training</summary>

Trainings are the secondary events in PET. They are not hosted often, but when they do, they take place usually in the PET private servers of the [PBCC ](https://www.roblox.com/games/17541193/Pinewood-Computer-Core) and [PBSTAC ](https://www.roblox.com/games/1564828419/PBST-Activity-Center?refPageId=cca962bf-9a13-40be-a66f-fa966e510261).

**You can earn up to 5 PET points from a normal training**.

</details>
</p>

<p>
<details>
<summary>Community Patrol</summary>

 The Community Patrol is a **large, monthly event** announced well in advance. It is hosted no earlier than the *25th of each month* (19th for February). It is the best chance PET gets each month to show off their skills and numbers.

The scheduling process is simple:

1. At the end of each month, a list of possible times will be posted.
2. Members of our **communications server** will be able to vote on the time that works best for them.
3. After voting is completed, the event- up to 45 minutes in length- will be scheduled and hosted at the voted-upon time.

**During a Community Patrol, PET are allowed to control and heat/cool the core, regardless of what temperature it is!**
**In order to bring together as many respondents as possible for the event, you can earn up to 7 points from the Community Patrol, with a possible bonus of 8!**

</details>
</p>

<p>
<details>
<summary>Operation MEGA: Flames Down!</summary>

The absolute ***largest, most team-based*** activity in PET! These events are very uncommon, no more than once a month but yield a large number of points. 

The idea is simple; a **large PET group** comes together and tackles multiple servers at the PBCC with the goal of resolving any and all disaster that may be occurring. Marshalls and above can host a server during this fun event.

**You can earn up to 9 points from Operation MEGA, with a possible bonus of 10!**

</details>
</p>

---

<p>
<details>
<summary>Hosting Rules</summary>


All hosts have to follow rules regarding hosting. These rules are seen below. 

* If the last event was a Patrol, one has to wait 3 hours in order to host another event
* If the last event was a Mini Patrol, one has to wait 2 hours in order to host another event
* If the last event was a Training, one has to wait 3 hours in order to host another event
* If the last event was a Community Patrol, one has to wait 5 hours in order to host another event
* If the last event was an OPERATION MEGA, one has to wait 7 hours in order to host another event
* Marshalls and Elite Respondents can schedule for up to 7 days in advance.
* PET, PBST and TMS hosts are not permitted to host an event 1 hour before and after a PET Operation MEGA, a PBST MEGA training or a TMS MEGA raid. 

**Group Patrols and Patrol Support events runs independent from the Marshall hosted events.** However, there are still rules relating to these events:
  * Each individual Elite Respondent can host one Group Patrol and Patrol Support event **once every two days**. 
  * The event limit for a Group Patrol cannot **exceed 30 minutes.**
  * A Group Patrol still cannot occur until **7 hours** after an Operation MEGA event has ended. 
  * A Group Patrol also cannot occur until **5 hours** after a Community Patrol has ended. 
  * A Group Patrol is allowed to be hosted soon after a Marshall hosted event such as a Joint Patrol. 
    However, a Marshall+ can still **overlap and overtake** a Group Patrol event if they wish to, however this isn't actively encouraged. 

Every hosts are expected to follow these hosting rules. Specialists and above have the authority to end a patrol early or postpone it should it violate said rules.

</details>
</p>

During all events, the following is required:

* One of the official [PET Fire ](https://www.roblox.com/catalog/6534085414/PET-Fire-Uniform-2021), [PET Medical ](https://www.roblox.com/catalog/6534090699/PET-Medical-Uniform-2021) or [PET HAZMAT ](https://www.roblox.com/catalog/6534090290/PET-Hazmat-Uniform-2021) uniforms- both shirt and pants. **If you prefer not to spend ROBUX on a uniform, free uniform morphs can be found at both PBSTAC and PBCC.**
* Respect for your fellow attendees and host.
* An attitude willing to participate and improve!

---

Toxicity and unsportsmanlike behaviour is extremely frowned upon, and may actually result in a person getting blacklisted from attending events not just in PET, but also PBST and TMS.

Teamswitching between PET, PBST and TMS are not allowed during official patrols or raids.

---

![image|690x470](/PET-Screenshot.jpg)

---

<div align="center">

## ***Groups you may encounter***

</div>

**The Mayhem Syndicate**

[The Mayhem Syndicate ](https://www.roblox.com/groups/4890641/The-Mayhem-Syndicate#!/about) is the raid group of PB and as such, will be a common adversary for any PET member. Certain rules need to be followed when dealing with Syndicate personnel, as well as when responding to their coordinated raids on the PBCC.

You may see the Syndicates at PET patrols. Unless they attack PET or set the cargo bays on fire, they are usually not on PET KOS. 

There are rare cases where a Syndicate goes rogue, and will attempt to save the core. These members pose no threat to PET.

<p>
<details>
<summary>TMS Raids</summary>

TMS Raids are another event PET can earn points from, but differ quite a bit from the norm in that they are actually a TMS event. PET must abide by a different set of rules and must respect all restrictions imposed.

TMS Raids are scheduled well beforehand however, the specific PBCC game server being raided is unknown until the time of the event. Once a raid begins, TMS will join the server and attempt to cause a meltdown, killing all PBST on sight by default. It is PET’s job to keep security forces healed however, this can often be easier said than done and because of that, PET personnel are better spent performing other tasks.
![image|690x373](/PBCC-Cargo-Bay.jpg)


**Effective PET duties at raids include** :

* Repairing the coolant pipe
* Keeping generators and coolant tanks filled, whether it’s via cargo train or respective button
* Quickly dealing with radiation leaks and power outages
* Suppressing obstructive fire spreads and engaging the Emergency Fire Suppression System when necessary
* Despawning or disrupting potentially threatening nuke trains and helping to lead evacuation efforts

Even at raids, TMS will not be KoS (Kill on Sight) unless stated otherwise, by a Marshall or above! That being said, TMS are always at automatic KOS ***ONCE*** the temp reaches **3000** or **-3000!**


</details>
</p>

<p>
<details>
<summary>Raid Response and Points</summary>

An official, pointed raid response will usually be held by either a Marshall or Specialist. It is your job as a PET responder to listen to the directions of the Marshall or Specialist.

**The amount of points that can be earned will vary depending on the raid’s level** :

* A Level 0- or Practice Raid typically awards no points, but one can be given as a bonus.
* A Level 1 Raid can award up to 4 points under normal attendance, with 5 being given as a bonus
* A Level 2 Raid can award up to 5 points under normal attendance, with 6 being given as a bonus
* A Level 3 Raid, commonly referred to as a MEGA Raid will usually have it’s pointing scale differ from MEGA to MEGA. For reference though, one can typically expect to earn 9 at most from normal attendance, with 10 or more being given as a bonus.

*Raid point scales, like all point scales are very flexible and one should always keep that in mind!*

</details>
</p>

![image|690x373](/TMS-Area.jpg)

---

In rare circumstances, **PET will not be welcome at a raid**. At the end of the day, raids are an event of TMS and due to that, others must respect the rules they decide to put into place (e.g. one raid, a host may prefer an all-out fight between TMS and PBST with all slots possible going to the fight). If PET is disallowed at a raid, you will know. It is your job as a respectful user to accept the fact and move on. Only a **tiny** fraction of the many, many raids hosted daily will impose rules restricting PET’s access and because of this, toxic and unsportsmanlike behavior on both sides regarding this matter will **not** be tolerated. Don’t cry and give a reason to restrict PET from more.

That being said, if you witness a TMS host being incredibly toxic to PET over the matter; report it to a TMS Instructor. 

Feel free to read the TMS handbook to gain more information on what they do: [The Mayhem Syndicate Handbook](https://devforum.roblox.com/t/the-mayhem-syndicate-handbook/595758)

---

**Pinewood Builders Security Team**

[Pinewood Builders Security Team](https://www.roblox.com/groups/645836/Pinewood-Builders-Security-Team#!/about) is the paramilitary force of Pinewood. Very often, they support PET in our mission to keep the PB facilities safe and protected. PET are expected to heal PBST members in the event that they are injured, especially during a Patrol Support session. 

During raids, PET can support PBST in attempting to heat/cool the core should the temperature reach +3000/-3000, on top of achieving other objectives. 

PBST are also authorised to help put out any fires or stop a hazmat spill if PET are not on site.

You can switch between being on-duty for PET or PBST by using the !setgroup command. PBST has their own set of tools, whilst on PET duty you may not use their tools. 

![PBST Area|690x373](/PBST-Area.jpg)



Feel free to read the PBST handbook to gain more information on what they do: [Pinewood Builders Security Team Handbook](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook/1612974)



---
 
This handbook is always a work in progress and will be updated as time goes.

---

**Signed,** <br>
***The Specialists & Chief***

![image|150x150, 50%](/PET-Logo.png)
